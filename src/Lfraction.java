// Kasutasin oma isa abi koodi kirjutamisel. 
//https://www.sitepoint.com/how-to-implement-javas-hashcode-correctly/

import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      // TODO!!! Your debugging tests here
	   Lfraction f1 = new Lfraction (200, -500);
	   System.out.println(f1);
	   Lfraction f2 = new Lfraction (-2, -5);
	   System.out.println(f2);
	   //Lfraction f3 = new Lfraction (-2, 0);
	   //System.out.println(f3);
	   //System.out.println(syt (200,400));
	   System.out.println(f1.inverse());
	   Lfraction f4 = Lfraction.toLfraction (Math.PI, 7);
	   System.out.println(f4);
	   Lfraction f5 = new Lfraction (22, 7);
	   System.out.println(f5);
      Lfraction f7 = new Lfraction (2, 5);
      Lfraction f8 = new Lfraction (4, 15);

	  System.out.println(f7.compareTo(f8));
	  System.out.println(valueOf ("4/x"));
   }
   
   private long nim;
   private long lug;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
	   nim=a;
	   lug=b;
	   
	   if (lug == 0) {
		   throw new RuntimeException("Murru nimetaja ei saa olla null!");
	   }
	   if (lug < 0) {
		  lug*=-1;
		  nim*=-1;		   
	   }
	   taanda ();
   }
 
   
   private static long syt(long a, long b){ // Eukleidese algoritm suurima �histeguri leidmiseks
	  if (b==0){
		  return a;
	  } else {
		  return syt (b,a%b);
	  }	  
   }
   
   private void taanda (){
	   long s= syt(Math.abs(nim), Math.abs(lug));
	   nim/= s;
	   lug/= s;
	   
   } 
   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return nim;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return lug;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
	   
      return Long.toString(nim) +"/"+Long.toString(lug); 
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
	  Lfraction other = (Lfraction) m;
	  if (nim==other.nim && lug==other.lug){
		return true;  
	  } 
	  return false;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {	   
	   int prime = 31;
	   int result = 1;
	   result = prime * result + Long.hashCode(lug);
	   result = prime * result + Long.hashCode(nim);
	   return result;
	   //Netist leitud algoritm collisioni v�ltimiseks. Viide �leval.
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
	   long v2k = (lug*m.lug)/syt(lug,m.lug);
	   long unim = nim*(v2k/lug);
	   long umnim = m.nim*(v2k/m.lug);
	  Lfraction a = new Lfraction (unim+umnim, v2k);
	  a.taanda();
      return a; 
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      return new Lfraction (nim*m.nim,lug*m.lug);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
	   if (nim==0){
		 throw new ArithmeticException("Nulliga jagamine ei l�he l�bi");
	   }
	  Lfraction l = new Lfraction(lug, nim);  
      return l;
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(nim*(-1), lug);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
	   if (m.nim==0){
		   throw new ArithmeticException("Proovid nulliga jagada.");
	   }
	   Lfraction jag = m.inverse();
      return times(jag);
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
	  Lfraction x = minus(m);
      return Long.signum(x.nim);
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {	   
      return new Lfraction(nim, lug); 
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {	   
      return nim/lug; 
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return new Lfraction(nim-integerPart()*lug, lug);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double)nim/(double)lug;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction (Math.round(((double)d*f)), d); 
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
	   String[] strmas = s.split("/");
	   if (strmas.length>2) {
		   throw new RuntimeException("S�ne "+s+" ei ole interpreteeritaval kujul.");
	   }
	   if(strmas[0].matches("[^0-9]+$")){
		   throw new RuntimeException("S�nes "+s+" ei ole interpreteeritav pikk t�isarv lugejaks.");
	   }
	   if(strmas[1].matches("[^0-9]+$")){
		   throw new RuntimeException("S�nes"+s+" ei ole interpreteeritav pikk t�isarv nimetajaks.");
	   }
	   if(Long.parseLong(strmas[1])==0){
		   throw new RuntimeException("Tekib nulliga jagamine."+s);
	   }
      return new Lfraction (Long.parseLong(strmas[0]),Long.parseLong(strmas[1])); 
   }
}

